BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA BETA  

You've been warned!

This is a -not LEAN PowerCLI based VEBA/KNative function running in at nearly 600 lines...

A very much not simple VEBA KN PowerCLI function to provide VM Guest OS phyical host core licensing compliance to a limited set of hosts in a cluster using vSphere DRS.
Allows for flexible compliance enforcement options.

I added triggers in the function.yml for vMotion events... watch for VEBA loading issues when isolating hosts or performing workflows that process large numbers of VM migrations.

TODO:
    Use Case can extend outside of VM to phyiscal host licensing fencing... remove all comments / names / remarks related to licensing and replace with terms related to VM affinit policy.
    Look for opportunities to simplify / streamline function code to reduce size of script as well as optomize execution time but keep it readable.
    Reduce Slack message verbosity - or add flag in config to control verbosity level for debugging / daily monitoring.

Quickstart...
- In vCenter
- - Create a vSphere service account for function to action with. 
- - - Make the service account an Administrator. Maybe later I'll deliniate least priv access requirements.
- - - - View everything, Assign Tags, Assign VM to DrsRule VMGroup, Stop-VM, Set-VM, Set-VMResourceConfiguration...
- - - Add the credentials to vcenter-secret.json
- - Setup DRS
- - - Create DRS VMHost, VM groups on all clusters with a standard name on ALL clusters.
- - - Create DRS VM/VMHost Affinity Rule with MustRunOn enforcement on ALL clusters to support the policy managed VMs.
- - Setup Tagging
- - - Create Tag Category for OS Licensing. ex: 'ManagedLicenses'
- - - Create Tags in this category for each OS you are managing. Ex: 'Windows', 'RHEL', etc.
- - - Create one more Tag in this category to tag on VMs that fail enforcement. Ex: 'NotCompliant'
- - Deside on a VM Name suffix that may be appended to the VM name if enforcement fails.
- Update values in 'vmlicensingaffinity-config.json.ps1' with what you created above.
- Setup Slack messaging
- - Create a Slack App Webhook
- - Update 'slack-secret.json' with your webook URI
- Setup KNative
- - Run 'createUpdateSecrets.ps1' to create required secrets
- - - It won't override any existing secrets except it's own config secret.
- - - Yes.. it is Powershell, but you can change to .sh easily if you want. Replace Write-Host with echo, export ns variable...
- Prepare to deploy function
- - Update Dockerfile to use your own ce-pcli-base image.
- - docker build -t yourRegistry/project/imagename:tag .
- - docker push yourRegistry/project/imagename:tag
- - Review function.yaml values.
- - - Update image to yourRegistry/project/imagename:tag
- Drumroll...
- - kubectl -f ./function apply

Once function starts you'll get a happy Slack message from Process-Init that it's logged into vCenter.

kubectl -n vmware-functions get deployments | grep vmaffinity | grep -v trigger
Note deployment name - update deployment name in next command as needed.
kubectl -n vmware-functions logs deployment/licensing-vmaffinity-00001-deployment -c user-container --tail 100 -f

You should now be tailing the pods log output.
Review log detail to ensure successful start and vCenter login.

Testing.... create a VM with OS type matching one of your policies filters.
Move VMs, change DrsRule options to break the policy, create / move VMs to watch non-compliant actions.
Reset configurations to be compliant again - move / clone VMs to watch remediation reversal execute.
Watch the log output for error details.

You'll also get Slack messaging for each triggered run. Not as verbose... maybe still too verbose; but easy to monitor.

