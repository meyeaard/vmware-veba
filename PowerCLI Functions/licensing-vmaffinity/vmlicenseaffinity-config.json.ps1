# I find PS structure easier to manipulate, use what you know.
# This also ensures that the PS handler script will properly parse it.
# This is a crutch to build config as PS and convert to JSON.

$psConfig = @{
    'policies' = @(
        #@{
        #    'filter' = '*Microsoft*'; 
        #          This is a PS '-like' filter string to match against $VM.ExtensionData.Config.GuestFullName property
        #    'description' = 'Optional description field';
        #    'notes' = 'Optional notes field'; 
        #           FYI, you can add ANY additional field in addition to what the script uses.
        #    'priority' = 1; 
        #           Priority of this policy - you can have multiple policies that match various OS strings, the one with the LOWEST number wins.
        #           With this you can have varying policies for say "*Microsoft*", "*Microsoft*Server*", and "*Microsoft*Server*2016*"
        #           Lowest priority number wins - so 0 beats 1, 1 beats 2, and so on.
        #    'drsVMHostRule' = "WindowsLicenseAffinity";
        #           Name of your clusters' DRS VMHostRule to place matching VMs.
        #    'tagCategory' ="ManagedLicenses";
        #    'tagName' = "Windows";
        #           Tag Category\Name to apply to matching VMs. ex "ManagedLicenses\Windows" or "ManagedLicenses\Oracle"
        #    'tagFail' = "NotCompliant";
        #           Tag to apply to VMs when enforcement fails. In same Tag Category as OS license tag.
        #    'vmFailSuffix' = "-NotLicenseCompliant";
        #           Used by 'rename' remediateMethod - String to append to VM Name if enforcement fails.
        #    'remediateMethods' = @(
        #       'rename', - Renames VM, appending vmFailSuffix string to name. Could be used with vCenter Alarm on VM name matching xyz.
        #       'zeroresources' - Sets hard maximum vCPU,vRAM,IOPS resource limits to 0 preventing guest OS from booting.
        #    );
        #    'resolveMethods' = @('rename','zeroresources')
        #       List of remediationMethods that can be auto reversed if placing VM on destination makes VM compliant.        
        #},

        @{
            'filter' = '*Microsoft*';
            'description' = 'Place Microsoft VMs on hosts allocated Windows Server Datacenter licensing.';
            'notes' = '';
            'priority' = 1;
            'drsVMHostRule' = "WindowsLicenseAffinity";
            'tagCategory' ="ManagedLicenses";
            'tagName' = "Windows";
            'tagFail' = "NotCompliant";
            'vmFailSuffix' = "-NotLicenseCompliant";
            'remediateMethods' = @(
                'rename','zeroresources','tag'
            );
            'resolveMethods' = @(
                'rename','zeroresources','tag'
            );
        },
        @{
            'filter' = '*Red Hat*';
            'description' = 'Place RHEL VMs on hosts allocated RHEL licensing.';
            'notes' = 'No enforcement policy here - some VMs with GuestFullName *RHEL* are really CentOS or other RHEL flavors.';
            'priority' = 1;
            'drsVMHostRule' = "RHELLicenseAffinity";
            'tagCategory' ="ManagedLicenses";
            'tagName' = "RHEL";
            'tagFail' = "NotCompliant";
            'vmFailSuffix' = "-NotLicenseCompliant";
            'remediateMethods' = @('none');
            'resolveMethods' = @('none');
        }        
    )
}
ConvertTo-JSON -InputObject $psConfig -Depth 10 | Set-Content -Path "vmlicenseaffinity-config.json"
