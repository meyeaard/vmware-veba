##################
### INITIALIZE ###
##################
Function Process-Init {
    Write-Host "Initializing Function"

    ## DISABLE PROGRESS BAR FOR HEADLESS OPERATION
    $ProgressPreference = "SilentlyContinue"

    # Create execution Id to correlate messages / log events.
    $execId = "$(Get-Date -Format "yyyyMMddHHmmssff"):"
    Write-Host "$($execId) Start Function Execution"

    # DEFINE VARIABLES
    $knFunctionName = ${env:K_CONFIGURATION}
    $knFunctionDeployment = ${env:HOSTNAME}
    $slackMessage = @()
    $slackMessage += "Function: $($knFunctionName)`nDeployment: $($knFunctionDeployment)`nExecution ID: $($execId)"

    # PARSE ENV JSON TO VARIABLES
    If(${env:VCENTER_SECRET}) {
        $vcSecret = ${env:VCENTER_SECRET} | ConvertFrom-Json
    } Else { 
        $Message = "K8s secret `$env:VCENTER_SECRET is not defined"
        $slackMessage += $Message
        Write-Host "$($execId) $Message"
        #break - Let it breack in VERIFY check for added detail
    }
    If (${env:AFFINITY_POLICIES}){
        $polAffinitySecret = ${env:AFFINITY_POLICIES} | ConvertFrom-Json
    } Else {
        $Message = "K8s secret `$env:AFFINITY_POLICIES is not defined"
        $slackMessage += $Message
        Write-Host "$($execId) $Message"
        #break - Let it breack in VERIFY check for added detail
    }
    If(${env:SLACK_SECRET}){
        $slackSecret = ${env:SLACK_SECRET} | ConvertFrom-Json
    } Else {
        $Message =  "K8s secret `$env:SLACK_SECRET is not defined"
        $slackMessage += $Message
        Write-Host "$($execId) $Message"
        #break - Let it breack in VERIFY check for added detail
    }   
    If(${env:FUNCTION_DEBUG} -eq "true") {
        Write-Host "DEBUG: K8s Secrets:"
        Write-Host "VCENTER_SECRET: ${env:VCENTER_SECRET}"
        Write-Host "AFFINITY_POLICIES: ${env:AFFINITY_POLICIES}"
        Write-Host "SLACK_SECRET ${env:SLACK_SECRET}"
    }

    ### LOCALLY SCOPE CONFIGURATION VARIABLES ###
    $vcHostname = ${vcSecret}.vCenter
    $vcUsername = ${vcSecret}.username
    $vcPassword = ${vcSecret}.password
    $slackWebhook = ${slackSecret}.SLACK_WEBHOOK_URL

    ### VERIFY CONFIGURATION VARIABLES ###
    $breakFlag = $false
    # VERIFY SLACK_SECRET
    If ((-not $slackWebhook)){
        $Message = "FATAL: Missing required slackWebhook from SLACK_SECRET"
        Write-Host "$($execId) $Message"
        $breakFlag = $true
    }
    # VERFIY VCENTER_SECRET
    If ((-not $vcHostname ) -or (-not $vcUsername ) -or (-not $vcPassword )){
        $missingMsg = @()
        If (-not $vcHostname){$missingMsg += 'vcHostname'}
        If (-not $vcUsername){$missingMsg += 'vcUsername'}
        If (-not $vcPassword){$missingMsg += 'vcPassword'}

        $Message = "FATAL: Missing required vCenter connection detail from VCENTER_SECRET secret: $($missingMsg -join(','))"
        $slackMessage += $Message
        Write-Host "$($execId) $Message"
        If(${env:FUNCTION_DEBUG} -eq "true") {
            Write-Host "Required values: vcenter, username, password"
            Write-Host "DEBUG: Variables passed from VCENTER_SECRET`n$(${vcSecret})"
        }
        $breakFlag = $true
    }
    # VERIFY AFFINITY_POLICIES
    If (${polAffinitySecret}.policies.Count -eq 0){
        $Message = "FATAL: No affinity policies found in AFFINITY_POLICIES secret."
        $slackMessage += $Message
        Write-Host "$($execId) $Message"
        If(${env:FUNCTION_DEBUG} -eq "true") {
            Write-Host "$($execId) DEBUG: Values passed from AFFINITY_POLICIES secret:`n$(${polAffinitySecret} | ConvertTo-JSON -Depth 10)"
        }
        $breakFlag = $true           
    } Else {
        $affPolicies = ${polAffinitySecret}.policies | Sort-Object -Property {$_.priority}
        Foreach ($affPol in $affPolicies){
            Write-Host "$($execId) Checking Affinity Policy: '$($affPol.filter)': " -NoNewline
            $missingMsg = @()
            If (-not $affPol.filter){$missingMsg += 'filter'}
            If (-not $affPol.priority){$missingMsg += 'priority'}
            If (-not $affPol.vmFailSuffix){$missingMsg += 'vmFailSuffix'}
            If (-not $affPol.filter){$drsVMHostRule += 'drsVMHostRule'}
            If (-not $affPol.tagCategory){$missingMsg += 'tagCategory'}
            If (-not $affPol.tagName){$missingMsg += 'tagName'}
            If (-not $affPol.remediateMethods){$missingMsg += 'remediateMethods'}
            If (-not $affPol.resolveMethods){$missingMsg += 'resolveMethods'}
            If ($missingMsg.Count -ne 0){
            Write-Host "Failed"
            $Message = "FATAL: License policy: '$($affPol.filter)' missing values: $($missingMsg -join(','))"
            $slackMessage += $Message
            Write-Host "$($execId) $Message"
            $breakFlag = $true
            } Else { 
            Write-Host "Success"
            }
        }
        If(${env:FUNCTION_DEBUG} -eq "true") {
            Write-Host "$($execId) DEBUG: Values passed from AFFINITY_POLICIES secret:`n$(${polAffinitySecret} | ConvertTo-JSON -Depth 10)"
        }
    }

    # Break if flag set
    If ($breakFlag){
        Send-SlackNotification -Webhook $slackWebhook -NotificationType 'critical' -Message ($slackMessage -join("`n"))
        break
    }

    # Connect to vCenter
    Write-Host "`n--- $(Get-Date -Format "yyyyMMdd HH:mm:ss K") ---"
    Write-Host "vCenter: $($vcHostname)"
    Import-Module VMware.VimAutomation.Core | Out-Null
    Set-PowerCLIConfiguration -ParticipateInCeip:$false  `
        -InvalidCertificateAction:Ignore `
        -DisplayDeprecationWarnings:$false `
        -Scope:User -Confirm:$false | Out-Null
    Try {
        Write-Host "Connecting to vCenter: $($vcHostname): " -NoNewline
        Connect-VIServer -Server $vcHostname -User $vcUsername -Password $vcPassword -Force:$true -ErrorAction:Stop
        Write-Host "Connected"
        $slackMessage += ":white_check_mark: vCenter $($vcHostname) Connected"
    } Catch {
        Write-Host "Failed`nERROR:`n$($error)"
        $Message = "Unable to connect to vCenter: $($vcHostname)"
        $slackMessage += ":no_entry: $($Message)"
        Write-Host "$($execId) $Message"
        Send-SlackNotification -Webhook $slackWebhook -NotificationType 'critical' -Message ($slackMessage -join("`n"))
        break
    }

    # Initialized - Send messages
    If($slackMessage){
        Send-SlackNotification -Webhook $slackWebhook -NotificationType 'normal' -Message ($slackMessage -join("`n"))
    }
}

################
### FUNCTION ###
################
Function Process-Handler {
   Param(
      [Parameter(Position=0,Mandatory=$true)][CloudNative.CloudEvents.CloudEvent]$CloudEvent
   )

   ## DISABLE PROGRESS BAR FOR HEADLESS OPERATION
   $ProgressPreference = "SilentlyContinue"

   # Create execution Id to correlate messages / log events.
   $execId = "$(Get-Date -Format "yyyyMMddHHmmssff"):"
   Write-Host "$($execId) Start Function Execution"

   # DEFINE VARIABLES
   $knFunctionName = ${env:K_CONFIGURATION}
   $knFunctionDeployment = ${env:HOSTNAME}
   $slackMessage = @()
   $Message = "Function: $($knFunctionName)`nDeployment: $($knFunctionDeployment)`nExecution ID: $($execId)"
   $slackMessage += $Message
   Write-Host "$($execId) $($Message.Replace("`n","`n$($execId) "))"
   $remediateVmFlag = $false

   # Ensure our vCenter connection is viable for Tag actions
   Try {
      Get-TagCategory -ErrorAction:stop | Out-Null
      Write-Host "$($execId) vCenter connection active"
   } Catch {
      $Message = "$(execId) vCenter connection expired. Reinitializing..."
      Write-Host "$($execId) $Message"
      $slackMessage += $Message
      Disconnect-VIServer -Server * -Confirm:$false -Force:$true
      Process-Init
   }

   # PARSE CloudEvent DATA
   $cloudEventData = $cloudEvent | Read-CloudEventJsonData -ErrorAction SilentlyContinue -Depth 10
   If($null -eq $cloudEventData) {
      $cloudEventData = $cloudEvent | Read-CloudEventData
   }
   If(${env:FUNCTION_DEBUG} -eq "true") {
      Write-Host "$($execId) DEBUG: CloudEventData`n $(${cloudEventData} | ConvertTo-Json)`n"
   }    

   # PARSE ENV JSON TO VARIABLES
   $vcSecret = ${env:VCENTER_SECRET} | ConvertFrom-Json
   $polAffinitySecret = ${env:AFFINITY_POLICIES} | ConvertFrom-Json
   $slackSecret = ${env:SLACK_SECRET} | ConvertFrom-Json

   # SHOW DEBUG DETAIL
   If(${env:FUNCTION_DEBUG} -eq "true") {
      Write-Host "$($execId) DEBUG: K8s Secrets:"
      Write-Host "$($execId) AFFINITY_POLICIES: ${env:AFFINITY_POLICIES}"
      Write-Host "$($execId) SLACK_SECRET: ${env:SLACK_SECRET}"
   }

   ### LOCALLY SCOPE CONFIGURATION VARIABLES ###
   $vcHostname = ${vcSecret}.vCenter
   $slackWebhook = ${slackSecret}.SLACK_WEBHOOK_URL

   # OUTPUT BASIC CLOUD DETAIL
   Write-Host "$($execId) Cloud Event:"
   Write-Host "$($execId)`tTime:    $($cloudEvent.Time)"
   Write-Host "$($execId)`tSource:  $($cloudEvent.Source)"
   Write-Host "$($execId)`tSubject: $($cloudEvent.Subject)"

   $Message = "vSphere Event: $($cloudEvent.Subject)"
   $slackMessage += $Message
   Write-Host "$($execId) $Message"

   # Get vSphere VM Object from CloudEvent provided VM Id
   $slackMessage += "vCenter: $($vcHostname)"
   $vmId = "VirtualMachine-$($cloudEventData.Vm.Vm.Value)"
   Write-Host "$($execId) Get-VM for Id: '$($vmId)': " -NoNewline
   Try {
      $vmObj = Get-VM -Id $vmId -ErrorAction:Stop
      Write-Host "Success"
   } Catch {
      Write-Host "ERROR`n$($error)"
      $Message = "ERROR: Unable to Get-VM on vmId: '$($vmId)'. Ending."
      $slackMessage += $Message
      Write-Host "$($execId) $Message"
      Send-SlackNotification -Webhook $slackWebhook -NotificationType 'critical' -Message ($slackMessage -join("`n"))
      break
   }

    $Message = "VM Name: '$($vmObj.Name)' ID: '$($vmId)'"
    $slackMessage += $Message
    Write-Host "$($execId) $Message"

    ## If VM GuestFullName matches policy filter... then apply policy
    # Sort affPolicies by priority sub property
    $affPolicies = ${polAffinitySecret}.policies | Sort-Object -Property {$_.priority}
    # Test for match on policy.filter and return policy with highest priority
    $affPolicy = $null
    $vmGuestFullName = $vmObj.ExtensionData.Config.GuestFullName 
    $affPolicy =  $affPolicies | Where-Object { $vmGuestFullName  -like $_.filter} | Select-Object -First 1

    If ($null -ne $affPolicy){

        ### DEFINE POLICY VARIABLES ###
        $polFilter = $affPolicy.filter
        $polPriority = $affPolicy.priority
        $polVMFailSuffix = $affPolicy.vmFailSuffix
        $polDrsVMHostRule = $affPolicy.drsVMHostRule
        $polTagName = $affPolicy.tagName
        $polTagFailName = $affPolicy.tagFail
        $polTagCategory = $affPolicy.tagCategory  
        $polRemediateMethods = $affPolicy.remediateMethods
        $polResolveMethods = $affPolicy.resolveMethods

        $Message = "Matched affinity policy filter '$($polFilter)' with priority '$($polPriority)' on '$($vmGuestFullName)' ."
        $slackMessage += $Message
        Write-Host "$($execId) $Message"

        Write-Host "$($execId) Get-Tag: '$($polTagCategory)/$($polTagName)': " -NoNewline
        Try {
            $tagObj = Get-Tag -Category $polTagCategory -Name $polTagName -ErrorAction:Stop
            Write-Host "Success"
            Write-Host "$($execId) Assign tag to VM: " -NoNewline
            Try {
                New-TagAssignment -Tag $tagObj -Entity $vmObj -Confirm:$false -ErrorAction:Stop
                Write-Host "Success"
                $Message = "VM assigned tag: '$($tagObj.Category)/$($tagObj.Name)'"
            } Catch {
                Write-Host "ERROR`n$($error)"
                $Message = "WARNING: Unable to assign tag to VM: '$($tagObj.Category)/$($tagObj.Name)'"
            }
            $slackMessage += $Message
            Write-Host "$($execId) $Message"
        } Catch {
            Write-Host "ERROR`n$($error)"
            $Message = "WARNING: Unable to Get-Tag  Category: '$($polTagCategory)/$($polTagName)'"
            $slackMessage += $Message
            Write-Host "$($execId) $Message"
        }
      
        # If VMHost is in a Cluster   
        If ($vmObj.VMhost.Parent.GetType().Name -eq 'ClusterImpl'){
            Write-Host "$($execId) Get-Cluster of VM: " -NoNewline
            # Get the Cluster Object
            $clusterObj = $vmObj.VMhost.Parent
            # If Cluster has DRS enabled
            If ($clusterObj.DrsEnabled){
                # Verify Drs Automation is Parial or Full and not Manual.
                If ($clusterObj.DrsAutomationLevel -ne "FullyAutomated"){
                    $Message = "WARNING: vSphere Cluster: '$($clusterObj.Name)' has DrsAutomationLevel '$($clusterObj.DrsAutomationLevel)' that will not fully enforce DRS Affinity rules."
                    $slackMessage += $Message
                    Write-Host "$Message"
                    $remediateVmFlag = $true
                }

                # Get the Cluster DrsVMHostRule
                Write-Host "$($execId) Get-DrsVMHostRule Named: '$($polDrsVMHostRule)' in Cluster: '$($clusterObj)': " -NoNewline
                Try {
                    $drsRule = Get-DrsVMHostRule -Name $polDrsVMHostRule -Cluster $clusterObj -ErrorAction:Stop
                    Write-Host "Success: $($drsRule)"
                } Catch {
                    Write-Host "Error`n$($error)"
                    $Message = "WARNING Unable to get DrsVMHostRule named: '$($polDrsVMHostRule)' in cluster: '$($clusterObj.Name)'"
                    $slackMessage += $Message
                    Write-Host "$($execId) $Message"
                    $remediateVmFlag = $true
                }

                Write-Host "$($execId) Verify DrsVMHostRule type is MustRunOn: " -NoNewline
                If ($drsRule.Type -ieq 'MustRunOn'){
                    Write-Host "Success: $($drsRule.Type)"
                } Else {
                    Write-Host "Fail: $($drsRule.Type)"
                    $Message = "WARNING: vSphere Cluster: '$($clusterObj.Name)' has DrsVMHostRule '$($drsRule.Name)' WITHOUT MustRunOn enforcement."
                    $slackMessage += $Message
                    Write-Host "$($execId) $Message"
                    $remediateVmFlag = $true
                }

                # Add VM to the DrsVMHostRule VMGroup
                Write-Host "$($execId) Add VM to DrsVMHostRule/VMGroup: '$($drsRule.Name)/$($drsRule.VMGroup.Name)': " -NoNewline
                Try {
                    Set-DrsClusterGroup -DrsClusterGroup $drsRule.VMGroup -VM $vmObj -Add -ErrorAction:Stop
                    Write-Host "Success"
                    $Message = "VM placed in DRS Affinity rule: '$($drsRule.Name)/$($drsRule.VMGroup.Name)'"
                } Catch {
                    Write-Host "Error: $($error)"
                    $Message = "WARNING: Unable to add VM to cluster: '$($clusterObj.Name)' DrsVMHostRule/VMGroup: '$($drsRule.Name)/$($drsRule.VMGroup)'"
                    $remediateVmFlag = $true
                }
                $slackMessage += $Message
                Write-Host "$($execId) $Message"

            } Else {
                ## DRS not enabled
                ## Does cluster have the tag assigned?
                If ($tagObj.Id -in ($clusterObj | Get-TagAssignment).Tag.Id) {
                    $Message = "Cluster not DRS enabled but has tag: '$($tagObj.Category)/$($tagObj.Name)'"
                } Else {
                    $Message = "WARNING: VM placed on cluster without DRS and without tag: '$($tagObj.Category)/$($tagObj.Name)'"
                    $remediateVmFlag = $true
                }
                $slackMessage += $Message
                Write-Host "$($execId) $Message"            
            }

        } Else {
            Write-Host "$($execId) Standalone VMHost (Not part of cluster)"
            Write-Host "$($execId) Check VMHost tag assignment: " -NoNewline
            If ($tagObj.Id -in (Get-TagAssignment -Entity $vmObj.VMHost).Tag.Id){
                Write-Host "Success"
                $Message = "Standalone VMHost is assigned tag: '$($tagObj.Category)/$($tagObj.Name)'"
            } Else {
                $Message = "WARNING: VM placed on a standalone VMHost: '$($vmObj.VMHost.Name)' that does not have tag: '$($tagObj.Category)/$($tagObj.Name)'"
                $remediateVmFlag = $true
            }
            $slackMessage += $Message
            Write-Host "$($execId) $Message"
        }

        ## If enforcement uncertain remediate to ensure compliance.
        ## Selection of which if any enforcements are used are defined in the AFFINITY_POLICIES secret.
        If ($remediateVmFlag){
            # Enforce method rename VM
            If ('rename' -in $polRemediateMethods){
                Write-Host "$($execId) Affinity Policy Remediation: Renaming VM: " -NoNewline
                Try {
                    Set-VM -VM $vmObj -Name "$($vmObj.Name)$polVMFailSuffix" -Confirm:$false -ErrorAction:Stop
                    Write-Host "Renamed"
                    $Message = "Enforcement: Renamed VM to '$($vmObj.Name)$polVMFailSuffix'."
                } Catch {
                    Write-Host "Failed`n$($error)"
                    $Message = "Enforcement: FAILED to rename VM to '$($vmObj.Name)$($polVMFailSuffix)' ERROR: ``` $($error) ``` "
                }
                $slackMessage += $Message
                Write-Host "$($execId) $Message"
            }

            # Enforcement method tag
            If ('tag' -in $polRemediateMethods){
                Write-Host "$($execId) Affinity Policy Remediation: Tagging VM"
                Write-Host "$($execId) Get-Tag '$($polTagCategory)/$($polTagFailName)': " -NoNewline
                Try {
                    $failTagObj = Get-Tag -Category $polTagCategory -Name $polTagFailName -ErrorAction:Stop
                    Write-Host "Success"
                    Write-Host "$($execId) Assign tag to VM: " -NoNewline
                    Try {
                        New-TagAssignment -Tag $failTagObj -Entity $vmObj -Confirm:$false -ErrorAction:Stop
                        Write-Host "Success"
                        $Message = "Enforcement: Tagged VM with: '$($failTagObj.Category)/$($failTagObj.Name)'"
                    } Catch {
                        Write-Host "FAILED"
                        $Message = "Enforcement: FAILED to assign tag to VM ERROR: ``` $($error) ``` "
                    }
                    $slackMessage += $Message
                    Write-Host "$($execId) $Message"
                } Catch {
                    Write-Host "FAILED"
                    $Message = "WARNING: Unable to Get-Tag: '$($polTagCategory)/$($polTagFailName)'"
                    $slackMessage += $Message
                    Write-Host "$($execId) $Message"
                }
            }

            # Enforce method 0 limit resources
            If ('zeroresources' -in $polRemediateMethods){
                Write-Host "$($execId) Affinity Policy Remediation: Setting 0 Limit on VM Resources (RAM, CPU): " -NoNewline
                Try {
                    $vmResConfig = Get-VMResourceConfiguration -VM $vmObj -ErrorAction:Stop
                    # Ensure VM is currently PoweredOff
                    If ((Get-VM -Id ($vmObj.Id)).PowerState -ne "PoweredOff"){
                        Stop-VM -vm $vmObj -Confirm:$false
                    }
                    Set-VMResourceConfiguration -Configuration $vmResConfig -MemLimitGB:0 -CpuLimitMhz:0 -ErrorAction:Stop -Confirm:$false
                    Write-Host "Success"
                    $Message = "Enforcement: Applied 0 Limits to vCPU Mhz and vRAM GB to ensure VM OS does not boot."
                } Catch {
                    Write-Host "FAILED`n$($error)"
                    $Message = "Enforcement: FAILED to 0 Limit VM Resources. ERROR: ``` $($error) ``` "
                }
                $slackMessage += $Message
                Write-Host "$($execId) $Message"
            } 
        } Else {
            ## VM is in compliance with VM Affinity Policies

            # If resolveMethods defined, reverse any remediation steps taken on VM previously.
            If ($polResolveMethods){
                # Enforce method rename VM
                If ('rename' -in $polResolveMethods){
                    # If VM.Name ends with vmFailSuffix, remove it.
                    If (($vmObj.Name).EndsWith($polVMFailSuffix)){
                        Write-Host "$($execId) Affinity Policy Resolved: Rename VM: " -NoNewline
                        Try {
                            Set-VM -VM $vmObj -Name $($vmObj.Name).Replace($polVMFailSuffix,'') -Confirm:$false -ErrorAction:Stop
                            Write-Host "Renamed"
                            $Message = "Enforcement Reversal: Removed VM vmFailSuffix VM Name: '$((Get-VM -Id $vmObj.Id).Name)'"
                        } Catch {
                            Write-Host "Failed`n$($error)"
                            $Message = "Enforcement Reversal: FAILED to remove VM vmFailSuffix ERROR: ``` $($error) ``` "
                        }
                        $slackMessage += $Message
                        Write-Host "$($execId) $Message"
                    }
                }

                # Enforcement method tag
                If ('tag' -in $polResolveMethods){
                    $vmTagAssignment = $null
                    If ($vmTagAssignment = Get-TagAssignment -Entity $vmObj -Category $polTagCategory | Where-Object {$_.Tag -eq "$($failTagObj.Category)/$($failTagObj.Name)"}){
                
                        Write-Host "$($execId) Affinity Policy Resolved: Tagging VM: "
                        Write-Host "$($execId) Get-Tag '$($polTagCategory)/$($polTagFailName)': " -NoNewline
                        Try {
                            $failTagObj = Get-Tag -Category $polTagCategory -Name $polTagFailName -ErrorAction:Stop
                            Write-Host "Success"
                            Write-Host "$($execId) Remove tag from VM: " -NoNewline
                            Try {
                                Remove-TagAssignment -TagAssignment $vmTagAssignment -Confirm:$false -ErrorAction:Stop
                                Write-Host "Success"
                                $Message = "Enforcement Reversal: Removed Tag from VM"
                            } Catch {
                                Write-Host "FAILED"
                                $Message = "Enforcement Reversal: FAILED to remove tag from VM ERROR: ``` $($error) ``` "
                            }
                            $slackMessage += $Message
                            Write-Host "$($execId) $Message"
                        } Catch {
                            Write-Host "FAILED"
                            $Message = "WARNING: Unable to Get-Tag: '$($polTagCategory)/$($polTagFailName)'"
                            $slackMessage += $Message
                            Write-Host "$($execId) $Message"
                        }
                    }
                }   

                # Enforce method 0 limit resources
                If ('zeroresources' -in $polResolveMethods){
                    Write-Host "$($execId) Affinity Policy Resolved: Removing 0 Limit on VM Resources (RAM, CPU): " -NoNewline
                    Try {
                       $vmResConfig = Get-VMResourceConfiguration -VM $vmObj -ErrorAction:Stop
                       Set-VMResourceConfiguration -Configuration $vmResConfig -MemLimitGB:$null -CpuLimitMhz:$null -ErrorAction:Stop -Confirm:$false
                       Write-Host "Success"
                       $Message = "Enforcement Reversal: Removed 0 Limits to vCPU Mhz and vRAM GB to ensure VM OS does not boot."
                    } Catch {
                       Write-Host "FAILED`n$($error)"
                       $Message = "Enforcement Reversal: FAILED to remove 0 Limit VM Resources: ``` $($error) ``` "
                    }
                    $slackMessage += $Message
                    Write-Host "$($execId) $Message"
                } 
            }
        }
        $slackMessage += "Execution Complete"

        # Send external messages
        $slackNotificationType = ''
        If ($remediateVmFlag){ 
            $slackNotificationType = 'critical'
            $slackMessage[0] = ":no_entry:`n$($slackMessage[0])"
        } ElseIf (($slackMessage -join(' ') -ilike "*FAILED*")){
            $slackNotificationType = 'error'
        } ElseIf (($slackMessage -join(' ')) -ilike "*WARNING*"){
            $slackNotificationType = 'warning'
        } Else {
            $slackNotificationType = 'normal'
            $slackMessage[0] = ":white_check_mark:`n$($slackMessage[0])"
        }
        Send-SlackNotification -Webhook $slackWebhook -NotificationType $slackNotificationType -Message ($slackMessage -join("`n"))

    } Else {
        Write-Host "$($execId) VM: '$($vmObj.Name)' Id: '$($vmObj.Id)' does not match an affinity policy. Nothing to do."
    }
    Write-Host "$($execId) End Function Execution`n"   
}

################
### SHUTDOWN ###
################
Function Process-Shutdown {

   Disconnect-VIServer -Server * -Confirm:$false -Force:$true
   Write-Host "Shutdown Complete"
}

#########################
## My Custom Functions ##
#########################
Function Send-SlackNotification{
   <# 
   .SYNOPSIS
       Send markdown notification message via Slack webhook.    
   
   .NOTES
       Name: Notify-Slack
       Author: Aaron Meyer
       Version: 1.0
       DateCreated: 06022021
   
   .EXAMPLE
       Send-SlackNotification -Webhook "https://hooks.slack.com...." -Message "The World is ending!" -NotificationType "critical"
       Send-SlackNotification -Webhook $wh -Message "Hello World" -NotificationType:normal
   
   .LINK
       N/A
    #>
    [CmdletBinding()]
    param(
        [Parameter(
            Mandatory = $true,
            HelpMessage = "Webhook URI from api.slack.com"
        )]$Webhook,
        [Parameter(
            Mandatory = $true,
            HelpMessage = "Markdown message text following Slack syntax."
        )][string[]] $Message,
        [Parameter(
            Mandatory = $false,
            HelpMessage = "[default] normal, warning, error, or critical"
        )]
        [ValidateSet("normal", "warning", "error", "critical")]
        [string[]] $NotificationType
    )

    # Prefix message based on notification type
    switch ($NotificationType){
        'normal' {}
        'warning' {$Message = ":warning: *Warning:*\n$($Message)"}
        'error' {$Message = ":mega: *ERROR:*\n$($Message)"}
        'critical' {$Message = ":rotating_light: *CRITICAL:*\n$($Message)"}
    }
    
    $Message = $Message.Replace('\n',"`n") # Convert newlines to PowerShell
    $body = @{blocks = @(@{ type = "section"; text = @{ type = "mrkdwn"; text = "$($Message)";}})} | ConvertTo-Json -Depth 5
    Invoke-WebRequest -Uri $($Webhook) -Method POST -ContentType "application/json" -Body $body
}
   
