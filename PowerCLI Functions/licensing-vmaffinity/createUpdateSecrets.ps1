# Create / Update secrets
# https://blog.atomist.com/updating-a-kubernetes-secret-or-configmap/

$ns="vmware-functions"

# vcenter-secret
# Fail if exists - it will be shared with other functions
kubectl -n $ns create secret generic vcenter-secret --from-file=VCENTER_SECRET=./vcenter-secret.json 
Write-Host "VERIFY: vcenter-secret"
kubectl -n $ns get secret vcenter-secret -o json | jq '.data | map_values(@base64d)'

# slack-secret
# Fail if exists - it will be shared with other functions
kubectl -n $ns create secret generic slack-secret --from-file=SLACK_SECRET=./slack-secret.json
Write-Host "VERIFY: vcenter-secret"
kubectl -n $ns get secret slack-secret -o json | jq '.data | map_values(@base64d)'

# vmlicensingaffinity-config
# Create / Update
kubectl -n $ns create secret generic licensing-vmaffinity-config --from-file=AFFINITY_POLICIES=./vmlicenseaffinity-config.json --dry-run=client -o yaml | kubectl apply -f -
Write-Host "VERIFY: vcenter-secret"
kubectl -n $ns get secret licensing-vmaffinity-config -o json | jq '.data | map_values(@base64d)'
