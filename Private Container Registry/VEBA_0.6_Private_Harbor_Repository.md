# Configure VMware VEBA 0.6.0 Fling (Knative) to use images from private registry using self signed or internal CA signed certificate.

The problem is that the registry's certificate must be trusted not only by docker, but also by the Knative serving controller.
As I was not able to locate a step by step process for resolving this online I am compiling my notes here for anyone who may find value.

_This is the summation of my own RTFM. This may not be the most effective way or best practices... If you have a better method, PLEASE, let me know._

**The resolution steps:**
1. Add signing CA public certificate to docker.
1. Add signing CA public certificate to Knative Controller service.
    - Needed for tag scanning even for public registries.
1. Add credentials for private regsitry pulls.
    - Only needed if you have a private registry.
    - This section can be used on it's own for publicly hosted private registries as well.

# Add CA certificate to docker
[Offical Docker Documentation](https://docs.docker.com/engine/security/certificates/)
#### Do this on the VEBA appliance AND your workstation
1. Install tool to rehash Photon OS keystore
    - `tdnf install -y openssl-c_rehash`
1. Add your CA public cert to the Photon OS keystore (.crt extension)
    - `cp ./myCA.cer /etc/ssl/certs/myCA.crt`
    - `rehash_ca_certificates.sh`
1. Create path for your registry certificate under /etc/docker/cert.d
    - `mkdir -p /etc/docker/cert.d/harbor.example.com`
1. Copy your CA public certificate to new path (.crt extension)
    - `cp ./myCA.cer /etc/docker/cert.d/harbor.example.com/myCA.crt`
1. Reboot VEBA appliance
    - Reboot appliance `reboot`
    - In my testing a reboot was required before I could `docker pull...` from the VEBA appliance. If there is a way to force the CA addition to go into effect immediately please comment.
1. (Optional) Do a `docker pull` from the VEBA appliance as root sourcing your private registry. You should not receive certificate errors.

# Add CA certificate to Knative serving controller
[Official Knative Documentation](https://knative.dev/docs/serving/tag-resolution/)
1. Setup kubectl on your workstation
    - Create local .kube folder.
      - `mkdir ~/.kube`
    - Copy kube config file from VEBA appliance.
      - `scp root@veba.example.com:/root/.kube/config ~/.kube/config`
    - Verify kubectl is connected to VEBA K8ts
      - `kubectl get all`
1. Create a secret in the knative-serving namespace to contain your CA certificate(s).
    - If you need to trust multiple certificates you can add additional --from-file.
    - `kubectl -n knative-serving create secret generic my-private-ca --from-file=./myCA.cer`
1. Create a YAML patch file to update the Knative serving controller. I have one here you can pull.
    - `curl -L https://gitlab.com/meyeaard/vmware-veba/-/raw/master/Private%20Container%20Registry/knative_controller_privateCA-patch.yaml -o knative_controller_privateCA.yaml`
        - Edit the patch if you use a different secret name or wish a different mount point.
1. Apply the patch to update the Knative service controller.
    - `kubectl -n knative-serving patch deployment controller --patch-file ./knative_controller_privateCA-patch.yaml`

**Now you should be able to pull images from public projects on your private registry using an internal CA or self signed certificate.**
**If you need to also pull from a private project you will need to continue on.**

# Configure private registry login
[Offical Knative documentation](https://knative.dev/docs/serving/deploying/private-registry/)

We are adding the credential to the vmware-functions namespace as that is where you will be publishing your VEBA functions. If you need to publish to other namesapces just adjust the namesapce names in the examples.

1. Create docker secret for your private registry in the vmware-functions namespace.
    - `kubectl -n vmware-functions create secret docker-registry 'private-registry' --docker-server='https://harbor.example.com' --docker-username='myUsername' --docker-password='myPassword' --docker-email='me@example.com'`
1. (Optional) Verify your secret.
    - `kubectl -n vmware-functions get secret private-registry --output=yaml`
    - `kubectl -n vmware-functions get secret private-registry --output=json | jq '.data | map_values(@base64d)'`
    - (Requires jq version 1.6 or greater)
1. Assign docker-secret to your deployments.
    - Option 1: Patch the default serviceaccount in the vmware-functions namespace to add your registry imagePullSecrets. This is a one time change for all functions deployed to this namespace.
        - `kubectl -n vmware-functions patch serviceaccount default -p "{\"imagePullSecrets\": [{\"name\": \"private-registry\"}]}"`
    - Option 2: Explicitly define imagePullSecrets in all of your service YAML with images sourced from the private registry. See example below.

Example from VEBA 0.6.0 Knative kn-ps-email example code modified to add imagePullSecrets.

      apiVersion: serving.knative.dev/v1
      kind: Service
      metadata:
        name: kn-ps-email
        namespace: vmware-functions
        labels:
          app: veba-ui
      spec:
        template:
          metadata:
          spec:
            imagePullSecrets:
            - name private-regsitry
            containers:
              - image: harbor.example.com/veba/kn-ps-email:1.3
                envFrom:
                  - secretRef:
                      name: email-secret
      ---
      apiVersion: eventing.knative.dev/v1
      kind: Trigger
      metadata:
        name: veba-ps-email-trigger
        namespace: vmware-functions
        labels:
          app: veba-ui
      spec:
        broker: default
        filter:
          attributes:
            type: com.vmware.event.router/event
            subject: VmRemovedEvent
        subscriber:
          ref:
            apiVersion: serving.knative.dev/v1
            kind: Service
            name: kn-ps-email









